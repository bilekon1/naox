
#ifndef NAOX_H
#define	NAOX_H

#include <iostream>
#include <sstream>

#include <XInput.h>
#include <Windows.h>

#include <alcommon/alproxy.h>
#include <alerror/alerror.h>
#include <alproxies/almotionproxy.h>

#include <qi/os.hpp>

using namespace std;

class Naox{
public:
	Naox(const string& ip, int port);
	void tick();

private:
	void initController();
	void initNao(const string& ip, int port);

	void checkButtons();
	void disableBodyPart();

	void setBody();
	void disableBody();
	void tickBody();

	void setLArm();
	void disableLArm();
	void tickLArm();

	void setRArm();
	void disableRArm();
	void tickRArm();

	void stiffnessOn(const string& name);
	void stiffnessOff(const string& name);

	void toggleLHand();
	void toggleRHand();
	void togglePause();

	void quit();
	
	unsigned int controllerNumber;
	unsigned int activeBodyPart;

	XINPUT_STATE state;
	AL::ALMotionProxy* motion;
	bool pause;
	bool LHandOpen;
	bool RHandOpen;
};

#endif	/* NAOX_H */