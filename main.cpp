
#include <iostream>

#include "naox.h"

using namespace std;

int main(int argc, char** argv) {

	cout << "[NAOX ] Starting..." << endl;
	Naox* nao = new Naox("127.0.0.1", 9559);

	while (1) {		
		nao->tick();
	}

}
