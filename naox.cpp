
#include "naox.h"

Naox::Naox(const string& ip, int port){
	activeBodyPart = 0;
	pause = true;
	LHandOpen = false;
	RHandOpen = false;

	initController();
	initNao(ip,port);
	
	motion->wakeUp();
	motion->rest();

	cout << "[NAOX ] NAO is paused. Press START and then select body part with arrows" << endl << endl;
}

void Naox::tick(){
	checkButtons();

	if(!pause){

		if	(state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER)		toggleLHand();
		if	(state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER)	toggleRHand();

		switch(activeBodyPart){
		case 1:
			tickBody();
			break;
		case 2:
			tickLArm();
			break;
		case 3:
			tickRArm();
			break;
		default:
			break;
		}
	}	
}

void Naox::initController(){
	memset(&state, 0, sizeof(XINPUT_STATE));

	for (int i = 0; i < 4; i++) {
		if (XInputGetState(i, &state) == ERROR_SUCCESS) {
			cout << "[NAOX ] Using Player " << i+1 << " controller" << endl;
			controllerNumber = i;
			return;
		}
	}

	cout << "[NAOX ] No controllers found. Exiting..." << endl << endl;
	system("pause");
	exit(1);
}

void Naox::initNao(const string& ip, int port) {
	try{

		cout << "[NAOX ] Connecting to: "<< ip << ":" << port << endl;
		AL::ALMotionProxy* init = new AL::ALMotionProxy(ip, port);
		cout << "[NAOX ] Connected" << endl;
		motion = init;		

	}catch (const AL::ALError& e){
		system("cls");
		cerr << "[NAOX ] Cannot connect to: " << ip << ":" << port << endl << endl;
		cerr << e.what() << endl;
		system("pause");
		exit(1);
	}

}

void Naox::checkButtons(){
	DWORD dwResult = XInputGetState( controllerNumber, &state );

	if	(state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP)		setBody();
	if	(state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT)		setLArm();	
	if	(state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT)	setRArm();	
	if	(state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK)			quit();
	if	(state.Gamepad.wButtons & XINPUT_GAMEPAD_START)			togglePause();	

}

void Naox::disableBodyPart(){
	switch(activeBodyPart){
	case 1:
		disableBody();
		return;
	case 2:
		disableLArm();
		return;
	case 3:
		disableRArm();
		return;
	default:
		return;
	}
}

void Naox::setBody(){
	//disableBodyPart();
	cout << "[NAOX ] ***BODY***" << endl;
	activeBodyPart = 1;
	stiffnessOn("HeadYaw");
	stiffnessOn("HeadPitch");	
}

void Naox::disableBody(){
	stiffnessOff("HeadYaw");
	stiffnessOff("HeadPitch");
	cout << "[NAOX ] ***DISABLE BODY***" << endl;
}

void Naox::tickBody(){
	DWORD dwResult = XInputGetState( controllerNumber, &state );

	float LX = state.Gamepad.sThumbLX / 100000.0f;
	float LY = state.Gamepad.sThumbLY / 100000.0f;
	float RX = state.Gamepad.sThumbRX / 100000.0f;
	float RY = state.Gamepad.sThumbRY / 100000.0f;

	if	(LX > 0.2f)		motion->changeAngles("HeadYaw", -0.1f, 0.2f);			
	if	(LX < -0.2f)	motion->changeAngles("HeadYaw", 0.1f, 0.2f);
	if	(LY > 0.2f)		motion->changeAngles("HeadPitch", -0.1f, 0.2f);			
	if	(LY < -0.2f)	motion->changeAngles("HeadPitch", 0.1f, 0.2f);	

	if (motion->moveIsActive()){
		if (RY < 0.2f && RY > -0.2f && RX < 0.2f && RX > -0.2f)		motion->stopMove();
	}else{
		if (RY > 0.2f)	motion->move(1.0f, 0.0f, 0.0f);
		if (RY < -0.2f)	motion->move(-1.0f, 0.0f, 0.0f);
		if (RX > 0.2f)	motion->move(0.0f, 0.0f, -1.0f);
		if (RX < -0.2f)	motion->move(0.0f, 0.0f, 1.0f);
	}


	//cout << " | " << LX << " | " << LY << endl;
	//qi::os::sleep(0.25f);
}

void Naox::setLArm(){
	cout << "[NAOX ] ***LEFT ARM***" << endl;
	//disableBodyPart();
	activeBodyPart = 2;
	stiffnessOn("LShoulderPitch");
	stiffnessOn("LShoulderRoll");
	stiffnessOn("LElbowYaw");
	stiffnessOn("LElbowRoll");
	stiffnessOn("LWristYaw");
	stiffnessOn("LHand");	
}

void Naox::disableLArm(){
	stiffnessOff("LShoulderPitch");
	stiffnessOff("LShoulderRoll");
	stiffnessOff("LElbowYaw");
	stiffnessOff("LElbowRoll");
	stiffnessOff("LWristYaw");
	stiffnessOff("LHand");
	cout << "[NAOX ] ***DISABLE LEFT ARM***" << endl;
}

void Naox::tickLArm(){
	DWORD dwResult = XInputGetState( controllerNumber, &state );

	float LX = state.Gamepad.sThumbLX / 100000.0f;
	float LY = state.Gamepad.sThumbLY / 100000.0f;
	float RX = state.Gamepad.sThumbRX / 100000.0f;
	float RY = state.Gamepad.sThumbRY / 100000.0f;

	int LT = state.Gamepad.bLeftTrigger;
	int RT = state.Gamepad.bRightTrigger;

	if	(LY > 0.2f)		motion->changeAngles("LShoulderPitch", -0.1f, 0.2f);			
	if	(LY < -0.2f)	motion->changeAngles("LShoulderPitch", 0.1f, 0.2f);
	if	(LX > 0.2f)		motion->changeAngles("LShoulderRoll", -0.1f, 0.2f);			
	if	(LX < -0.2f)	motion->changeAngles("LShoulderRoll", 0.1f, 0.2f);	

	if	(RX > 0.2f)		motion->changeAngles("LElbowYaw", 0.1f, 0.2f);			
	if	(RX < -0.2f)	motion->changeAngles("LElbowYaw", -0.1f, 0.2f);
	if	(RY > 0.2f)		motion->changeAngles("LElbowRoll", -0.1f, 0.2f);			
	if	(RY < -0.2f)	motion->changeAngles("LElbowRoll", 0.1f, 0.2f);

	if	(LT > 30)		motion->changeAngles("LWristYaw", -0.025f, 0.2f);			
	if	(RT > 30)		motion->changeAngles("LWristYaw", 0.025f, 0.2f);	
}

void Naox::setRArm(){
	cout << "[NAOX ] ***RIGHT ARM***" << endl;
	//disableBodyPart();
	activeBodyPart = 3;
	stiffnessOn("RShoulderPitch");
	stiffnessOn("RShoulderRoll");
	stiffnessOn("RElbowYaw");
	stiffnessOn("RElbowRoll");
	stiffnessOn("RWristYaw");
	stiffnessOn("RHand");	
}

void Naox::disableRArm(){
	stiffnessOff("RShoulderPitch");
	stiffnessOff("RShoulderRoll");
	stiffnessOff("RElbowYaw");
	stiffnessOff("RElbowRoll");
	stiffnessOff("RWristYaw");
	stiffnessOff("RHand");
	cout << "[NAOX ] ***DISABLE RIGHT ARM***" << endl;
}

void Naox::tickRArm(){
	DWORD dwResult = XInputGetState( controllerNumber, &state );

	float LX = state.Gamepad.sThumbLX / 100000.0f;
	float LY = state.Gamepad.sThumbLY / 100000.0f;
	float RX = state.Gamepad.sThumbRX / 100000.0f;
	float RY = state.Gamepad.sThumbRY / 100000.0f;

	int LT = state.Gamepad.bLeftTrigger;
	int RT = state.Gamepad.bRightTrigger;

	if	(LY > 0.2f)		motion->changeAngles("RShoulderPitch", -0.1f, 0.2f);			
	if	(LY < -0.2f)	motion->changeAngles("RShoulderPitch", 0.1f, 0.2f);
	if	(LX > 0.2f)		motion->changeAngles("RShoulderRoll", -0.1f, 0.2f);			
	if	(LX < -0.2f)	motion->changeAngles("RShoulderRoll", 0.1f, 0.2f);	

	if	(RX > 0.2f)		motion->changeAngles("RElbowYaw", 0.1f, 0.2f);			
	if	(RX < -0.2f)	motion->changeAngles("RElbowYaw", -0.1f, 0.2f);
	if	(RY > 0.2f)		motion->changeAngles("RElbowRoll", -0.1f, 0.2f);			
	if	(RY < -0.2f)	motion->changeAngles("RElbowRoll", 0.1f, 0.2f);

	if	(LT > 30)		motion->changeAngles("RWristYaw", -0.025f, 0.2f);			
	if	(RT > 30)		motion->changeAngles("RWristYaw", 0.025f, 0.2f);	
}

void Naox::stiffnessOn(const string& joint){
	motion->stiffnessInterpolation(joint, 1.0f, 0.5f);
}

void Naox::stiffnessOff(const string& joint){
	motion->stiffnessInterpolation(joint, 0.0f, 0.5f);
}

void Naox::toggleLHand(){
	if(LHandOpen){
		motion->closeHand("LHand");
		LHandOpen = false;
	}else{
		motion->openHand("LHand");
		LHandOpen = true;
	}
}

void Naox::toggleRHand(){
	if(RHandOpen){
		motion->closeHand("RHand");
		RHandOpen = false;
	}else{
		motion->openHand("RHand");
		RHandOpen = true;
	}
}

void Naox::togglePause(){
	if(pause){		
		pause = false;
		cout << "[NAOX ] ***RESUME***" << endl;
		motion->wakeUp();
		motion->moveInit();
	}else{		
		pause = true;
		cout << "[NAOX ] ***PAUSE***" << endl;
		motion->rest();
	}
}

void Naox::quit(){
	//disableBodyPart();
	cout << "[NAOX ] ***EXIT***" << endl;
	motion->wakeUp();
	motion->rest();
	cout << "[NAOX ] Exitting on user request" << endl << endl;
	system("pause");
	exit(0);
}